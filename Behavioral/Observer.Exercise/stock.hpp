#ifndef STOCK_HPP_
#define STOCK_HPP_

#include <string>
#include <iostream>
#include <set>

class Stock;

class Observer
{
public:
    virtual void update(Stock* stock) = 0;
    virtual ~Observer()
    {
    }
};

// Subject
class Stock
{
private:
	std::string symbol_;
	double price_;
    std::set<Observer*> observers_;
public:
	Stock(const std::string& symbol, double price) : symbol_(symbol), price_(price)
	{

	}

	std::string get_symbol() const
	{
		return symbol_;
	}

	double get_price() const
	{
		return price_;
	}

	// TODO: rejestracja obserwatora
    void attach(Observer* observer)
    {
        observers_.insert(observer);
    }


	// TODO: wyrejestrowanie obserwatora
    void detach(Observer* observer)
    {
        observers_.erase(observer);
    }


	void set_price(double price)
	{
        if (price != price_)
        {
            price_ = price;
            notify();
        }

		// TODO: powiadomienie inwestorow o zmianie kursu...
	}
protected:
    void notify()
    {
        for(auto o : observers_)
        {
            o->update(this);
        }
    }
};

class Investor : public Observer
{
	std::string name_;
public:
	Investor(const std::string& name) : name_(name)
	{
	}

    void update(Stock* stock)
	{
        std::cout << name_ << " notified; Symbol: " << stock->get_symbol()
                  << "; Price: " << stock->get_price() << std::endl;
	}
};

#endif /*STOCK_HPP_*/
