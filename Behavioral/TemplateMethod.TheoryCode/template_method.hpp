#ifndef TEMPLATE_METHOD_HPP_
#define TEMPLATE_METHOD_HPP_

#include <iostream>
#include <string>
#include <memory>

using namespace std;

class Product
{
public:
    virtual void run()
    {
        cout << "Product::run()\n";
    }

    virtual ~Product() {}
};

class BetterProduct : public Product
{
public:
    virtual void run()
    {
        cout << "BetterProduct::run()\n";
    }
};

// "AbstractClass"
class AbstractClass
{
protected:
	virtual void primitive_operation_1() = 0;
	virtual void primitive_operation_2() = 0;
    virtual Product* create_product() = 0;
    virtual void hook_method() {}
    virtual bool is_valid() const
    {
        return true;
    }
public:
	void template_method()
	{
		primitive_operation_1();
        if (is_valid())
            primitive_operation_2();
        std::unique_ptr<Product> product(create_product());
        product->run();
        hook_method();
		std::cout << std::endl;
	}
	
	virtual ~AbstractClass() {}
};

// "ConcreteClass"
class ConcreteClassA : public AbstractClass
{
protected:
	void primitive_operation_1()
	{
		std::cout << "ConcreteClassA.PrimitiveOperation1()" << std::endl;
	}

	void primitive_operation_2()
	{
		std::cout << "ConcreteClassA.PrimitiveOperation2()" << std::endl;
	}

    Product* create_product()
    {
        return new BetterProduct();
    }
};

// "ConcreteClass"
class ConcreteClassB : public AbstractClass
{
protected:
	void primitive_operation_1()
	{
		std::cout << "ConcreteClassB.PrimitiveOperation1()" << std::endl;
	}

	void primitive_operation_2()
	{
		std::cout << "ConcreteClassB.PrimitiveOperation2()" << std::endl;
	}

    void hook_method()
    {
        cout << "Hook..." << endl;
    }

    Product* create_product()
    {
        return new Product();
    }
};

#endif /*TEMPLATE_METHOD_HPP_*/
